;;; nofrils-dark-theme.el --- A monochromatic, purple-gray theme

;; Copyright (C) 2020 Brihadeesh S (@gitlab/peregrinator; @github/peregrinat0r)
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;; Author: Brihadeesh S <brihadeesh@protonmail.com>
;; URL: https://gitlab.com/peregrinator/nofrils.el
;; Version: 0.2
;; Package-Requires: ((colourless-themes "0.2"))
;; License: MIT
;; Keywords: faces theme

;;; Commentary:
;; This is a port of nofrils-dark, a nearly colourless vim theme by Robert Melton
;; https://github.com/robertmeta/nofrils;
;; Made with the forked version of the `colourless-themes` macro
;; https://github.com/peregrinat0r/colourless-themes.el

;;; Code:
(require 'colourless-themes)

(deftheme nofrils-dark "A monochromatic purple-gray theme")

(colourless-themes-make nofrils-dark
                       "#262626"    ; bg
                       "#262626"    ; bg+
                       "#303030"    ; current-line
                       "#6c6c6c"    ; fade
                       "#eeeeee"    ; fg
                       "#00ffff"    ; fg+
                       "#6c6c6c"    ; primary
                       "#ff00ff"    ; red
                       "#800000"    ; orange
                       "#808000"    ; yellow
                       "#00ff00")   ; green

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'nofrils-dark)
(provide 'nofrils-dark-theme)
;;; nofrils-dark-theme.el ends here
