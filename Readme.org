#+TITLE: Colourless themes
#+AUTHOR: Brihadeesh S (They/Them)
#+EMAIL: brihadeesh@protonmail.com

* Boring themes for the elitist

`colourless-themes` is a collection of “primarily colourless themes".

** Installation

Install this how you would install a package from github - this varies
with the flavour of Emacs or the setup in use. If you use a
Frankenstein's monster like I use, do:

#+BEGIN_SRC emacs-lisp :tangle no
  (use-package colourless-themes
    :straight (:host gitlab
     :repo "peregrinator/colourless-themes-el")

    :config
    ;;optionally loading a theme
    (load-theme 'einkless t)
#+END_SRC

** Usage

Temporarily set it the ~M-x load-theme~ , or via ~M-x
customize-themes~. To make it persistent, see [[#Installation][Installation]]. Disable
any other themes to ensure a colourless experience.

* Featured themes

Currently, this repository features:

- ~nordless~, dark and blue, inspired by [[https://github.com/arcticicestudio/nord][nord]]
- ~hydrangealess~, dark and magenta, inspired by [[https://github.com/yuttie/hydrangea-emacs][hydrangea]]
- ~seagreenless~, light and seagreen
- ~lavenderless~, purple and mint, inspired by [[https://github.com/emacsfodder/emacs-lavender-theme/][Lavender]]
- ~nofrils-darkless~, a clone of [[https://github.com/robertmeta/nofrils][nofrils-dark]]
- ~darkless~, dark and white, inspired by nofrils, but with less
  colours
- ~chocolateless~, chocolate taste, inspired by [[https://github.com/SavchenkoValeriy/emacs-chocolate-theme][chocolate]]
- ~einkless~, light theme, inspired by [[https://github.com/maio/eink-emacs][eink]]
- ~broceliande~, green and cyan

Additionally check out other themes I've made with this macro:

- *Beelzebub* A dark, purplish colourscheme based on Stanislav
  Karkavin's vim theme: [[https://github.com/xdefrag/vim-beelzebub][vim-beelzebub]]
- *mephistopheles* an original dark theme with yellow and green
  foreground, again inspired by Stanislav Karkavin's vim theme
  mentioned above.
- *nofrils*, Emacs ports /faithful/ to the [[https://github.com/robertmeta/nofrils][original nofrils
  themes]]. These have a few more colours than the others.

** Screenshots
*WIP*

* This is work in progress!
